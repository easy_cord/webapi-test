﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment
{
    public class Constants
    {
        public class DbText
        {
            public const string CustomerName = "@CustomerName";
            public const string Address = "@Address";
            public const string DOB = "@DOB";
            public const string Gender = "@gender";
            public const string Saledate = "@saledate";
        }
        
    }
}
