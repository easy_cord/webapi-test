﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Assignment.Model;
using Dataaccess.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic;


namespace Assignment.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContractController : ControllerBase
    {
        private readonly ILogger<ContractController> _logger;

        public ContractController(ILogger<ContractController> logger)
        {
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok();
        }

        [HttpPost]
        public IActionResult Post(contract obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>() {
                    new SqlParameter(Constants.DbText.CustomerName, SqlDbType.VarChar) { Value = obj.customername },
                    new SqlParameter(Constants.DbText.Address, SqlDbType.VarChar) { Value = obj.address },
                    new SqlParameter(Constants.DbText.DOB, SqlDbType.DateTime) { Value = obj.DOB },
                    new SqlParameter(Constants.DbText.Gender, SqlDbType.VarChar) { Value = obj.gender },
                    new SqlParameter(Constants.DbText.Saledate, SqlDbType.DateTime) { Value = obj.saledate }
            };

            var dbContext = new AssignmentContext();
            using (var command = dbContext.Database.GetDbConnection().CreateCommand())
            {
                try
                {


                    command.Parameters.AddRange(parameters.ToArray());
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "saveContact";

                    dbContext.Database.OpenConnection();
                    command.ExecuteNonQuery();

                }
                finally
                {
                    dbContext.Database.CloseConnection();
                }
            }

            return Ok();
        }


        [HttpPut]

        public IActionResult Put(contract obj)
        {
            //Contracts contract = new Contracts()
            //{
            //    Id= obj.id,
            //    CustomerName = obj.customername,
            //    Address = obj.address,
            //    Gender = obj.gender,
            //    Dob = obj.DOB,
            //    SaleDate = obj.saledate,
            //};
            var dbContext = new AssignmentContext();

            dbContext.Update(new Contracts()
            {
                Id = obj.id,
                CustomerName = obj.customername,
                Address = obj.address,
                Gender = obj.gender,
                Dob = obj.DOB,
                SaleDate = obj.saledate,
            });
            dbContext.SaveChanges();

            return Ok();
        }

        [HttpDelete]

        public IActionResult Delete(contract obj)
        {
            using (var dbContext = new AssignmentContext())
            {

                var std = new Contracts()
                {
                    Id = obj.id
                };

                dbContext.Remove(std);
                dbContext.SaveChanges();
            }

            return Ok();
        }

    }
}
