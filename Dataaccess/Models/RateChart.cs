﻿using System;
using System.Collections.Generic;

namespace Dataaccess.Models
{
    public partial class RateChart
    {
        public int? Id { get; set; }
        public int? PlanId { get; set; }
        public string Gender { get; set; }
        public int? Age { get; set; }
        public decimal? NetPrice { get; set; }
    }
}
