﻿using System;
using System.Collections.Generic;

namespace Dataaccess.Models
{
    public partial class Contracts
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public string Country { get; set; }
        public DateTime? Dob { get; set; }
        public DateTime? SaleDate { get; set; }
        public int? CoveragePlan { get; set; }
        public decimal? NetPrice { get; set; }
    }
}
