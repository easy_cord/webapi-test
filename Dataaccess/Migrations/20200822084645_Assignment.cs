﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Dataaccess.Migrations
{
    public partial class Assignment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Contracts",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    CustomerName = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Address = table.Column<string>(unicode: false, maxLength: 500, nullable: true),
                    Gender = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    Country = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    DOB = table.Column<DateTime>(type: "datetime", nullable: true),
                    SaleDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    CoveragePlan = table.Column<int>(nullable: true),
                    NetPrice = table.Column<decimal>(type: "decimal(18, 0)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contracts", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "RateChart",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: true),
                    PlanID = table.Column<int>(nullable: true),
                    Gender = table.Column<string>(maxLength: 50, nullable: true),
                    Age = table.Column<int>(nullable: true),
                    NetPrice = table.Column<decimal>(type: "decimal(18, 0)", nullable: true)
                },
                constraints: table =>
                {
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contracts");

            migrationBuilder.DropTable(
                name: "RateChart");
        }
    }
}
